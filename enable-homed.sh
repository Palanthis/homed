#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

cp nss-auth /etc/pam.d/nss-auth

mv /etc/pam.d/system-auth /etc/pam.d/old-system-auth

cp system-auth /etc/pam.d/system-auth

systemctl enable systemd-homed.service
